# Read Model HDF5 to JSON
Read a CNN model from a HDF5 format and make a JSOn with the parameters and model of CNN.

## Build with 🛠️
* [Jupyter] - Notebook JUpyter
* [Python] - Python 3.8.10

## Authors ✒️
**Team Embedded AI -- PUJ**

**Juan Sebastián Barreto Jiménez** - *Main team* - [jsebastianbarretoj99](https://gitlab.com/jsebastianbarretoj99)

**María José Niño Rodríguez** - *Main team* - [mjninor99](https://gitlab.com/mjninor99)

**Juan Camilo Devia Bastos** - *Main team* - [juandevia](https://gitlab.com/juandevia)

**Janet Chen He** - *Main team* - [XingYi98](https://gitlab.com/XingYi98)

## License 📄
This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.