import h5py
import numpy as np
import struct
import sys

if len(sys.argv) != 2:
    print("Number of parameters is incorrect for",sys.argv[0],"it need one parameter name of files")
    print("Example:","LeNet_Digits")
    exit()

name_network = sys.argv[1]
filenameIn = 'input/'+ name_network +'_weights.hdf5'
filenameOutCfg = 'output/parameters_'+ name_network +'.mjs'
filenameOutJson = 'output/parameters_'+ name_network +'.json'
filename_out_header = 'output/parameters_'+ name_network +'.h'

## HDF5 to MJS

# open the HDF5 file that contains the model weights
f = h5py.File(filenameIn, 'r')

# create layer name list
if name_network == "LeNet_Digits":
    ordered_layers = ['conv2d', 'conv2d_1', 'dense','dense_1']
elif name_network == "LeNet_Objects":
    ordered_layers = ['conv2d_3', 'conv2d_4','conv2d_5', 'dense_2','dense_3']
elif name_network == "VGG_Emotions":
    ordered_layers = [
                        'conv2d_6', 'conv2d_7', 'batch_normalization_5',
                        'conv2d_8', 'conv2d_9', 'batch_normalization_6',
                        'conv2d_10', 'conv2d_11', 'batch_normalization_7',
                        'dense_3', 'batch_normalization_8', 'dense_4',
                        'batch_normalization_9', 'dense_5'              
                    ]
elif name_network == "LSTM_Audio_Digits" or name_network == "LSTM_Audio_Digits2":
    ordered_layers = ['lstm_0','dense_0', 'dense_1','dense_2']
    
layers_name = [name for name in f.keys()]

#get the weight names and values
layers_info = []

biases = []
weights = []
recurrent_weights = []
gammas = []
betas = []
moving_mean = []
moving_variance =[]

header = open(filename_out_header, 'w')

header.write('#ifndef PARAMETERS_'+ name_network.upper() +'_H\n')
header.write('#define PARAMETERS_'+ name_network.upper() +'_H\n\n')

for layer_pos in ordered_layers:
    if layer_pos in layers_name:
        layer = f[layer_pos]
        parameters_names = layer.attrs['weight_names']
        parameters = [layer[parameter_name][:] for parameter_name in parameters_names]
        for name, parameter in zip(parameters_names, parameters):
            if 'conv' in layer.name:
                if 'kernel' in name:
                    kernel = np.transpose(parameter, [3, 2, 0, 1])
                    kernel_flatten = kernel.flatten()
                    header.write('const float '+ layer_pos + '_w['+ str(kernel_flatten.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in kernel_flatten))
                    header.write('};\n\n')
                    weights.append(kernel)
                elif 'bias' in name:
                    header.write('const float '+ layer_pos + '_b['+ str(parameter.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in parameter))
                    header.write('};\n\n')
                    biases.append(parameter)
                    layers_info.append({'layer':layer.name,'w':weights, 'b':biases})
                    biases = []
                    weights = []
            elif 'dense' in layer.name:
                if 'kernel' in name:
                    kernel = np.transpose(parameter, [1, 0])
                    kernel_flatten = kernel.flatten()
                    header.write('const float '+ layer_pos + '_w['+ str(kernel_flatten.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in kernel_flatten))
                    header.write('};\n\n')
                    weights.append(kernel)
                elif 'bias' in name:
                    header.write('const float '+ layer_pos + '_b['+ str(parameter.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in parameter))
                    header.write('};\n\n')
                    biases.append(parameter)
                    layers_info.append({'layer':layer.name,'w':weights, 'b':biases})
                    biases = []
                    weights = []
            elif 'batch' in layer.name:
                if 'gamma' in name:
                    header.write('const float '+ layer_pos + '_g['+ str(parameter.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in parameter))
                    header.write('};\n\n')
                    gammas.append(parameter)
                elif 'beta' in name:
                    header.write('const float '+ layer_pos + '_be['+ str(parameter.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in parameter))
                    header.write('};\n\n')
                    betas.append(parameter)
                elif 'moving_mean' in name:
                    header.write('const float '+ layer_pos + '_m_m['+ str(parameter.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in parameter))
                    header.write('};\n\n')
                    moving_mean.append(parameter)
                elif 'moving_variance' in name:
                    header.write('const float '+ layer_pos + '_m_v['+ str(parameter.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in parameter))
                    header.write('};\n\n')
                    moving_variance.append(parameter)
                    layers_info.append({'layer':layer.name,'g':gammas, 'be':betas, 'm_m':moving_mean, 'm_v':moving_variance})
                    gammas = []
                    betas = []
                    moving_mean = []
                    moving_variance =[]
            elif 'lstm' in layer.name:
                if 'kernel' in name and 'recurrent' not in name:
                    kernel = np.transpose(parameter, [1, 0])
                    kernel_flatten = kernel.flatten()
                    header.write('const float '+ layer_pos + '_w['+ str(kernel_flatten.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in kernel_flatten))
                    header.write('};\n\n')
                    weights.append(np.transpose(parameter, [1, 0]))
                elif 'recurrent' in name:
                    kernel = np.transpose(parameter, [1, 0])
                    kernel_flatten = kernel.flatten()
                    header.write('const float '+ layer_pos + '_r_w['+ str(kernel_flatten.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in kernel_flatten))
                    header.write('};\n\n')
                    recurrent_weights.append(kernel)
                elif 'bias' in name:
                    header.write('const float '+ layer_pos + '_b['+ str(parameter.size) +'] = ')
                    header.write('{')
                    header.write(', '.join(str(x) for x in parameter))
                    header.write('};\n\n')
                    biases.append(parameter)
                    layers_info.append({'layer':layer.name, 'w':weights, 'b':biases, 'r_w':recurrent_weights})
                    biases = []
                    weights = []
                    recurrent_weights = []

header.write('\n#endif\n')

header.close()

print("File created with parameters of", filenameIn, "in", filename_out_header)


with open(filenameOutCfg, 'wb') as f:
    f.write(struct.pack('i', len(ordered_layers)))
    for layer_info in layers_info:
        print(layer_info['layer'][1])
        f.write(bytes(layer_info['layer'][1], encoding="utf-8"))
        if 'conv' in layer_info['layer'] or 'dense' in layer_info['layer']:
            size_weights = 1
            for index in layer_info['w'][0].shape:
                size_weights *= index
            f.write(struct.pack('i', size_weights))
            for weight in layer_info['w'][0].flatten():
                f.write(struct.pack('f', weight))
            
            # bias
            size_biases = len(layer_info['b'][0])
            f.write(struct.pack('i', size_biases))
            if size_biases != 0:
                for bias in layer_info['b'][0]:
                    f.write(struct.pack('f', bias))     

        elif 'batch' in layer_info['layer']:
            # beta
            size_batch = len(layer_info['be'][0])
            f.write(struct.pack('i', size_batch))
            for beta in layer_info['be'][0]:
                f.write(struct.pack('f', beta))

            # gamma
            for gamma in layer_info['g'][0]:
                f.write(struct.pack('f', gamma))

            #moving mean
            for mov_mean in layer_info['m_m'][0]:
                f.write(struct.pack('f', mov_mean))
            
            # moving variance
            for mov_variance in layer_info['m_v'][0]:
                f.write(struct.pack('f', mov_variance))

        elif 'lstm' in layer_info['layer']:
            size_weights = 1
            for index in layer_info['w'][0].shape:
                size_weights *= index
            f.write(struct.pack('i', size_weights))
            for weight in layer_info['w'][0].flatten():
                f.write(struct.pack('f', weight))
            size_biases = len(layer_info['b'][0])
            f.write(struct.pack('i', size_biases))
            if size_biases != 0:
                for bias in layer_info['b'][0]:
                    f.write(struct.pack('f', bias))
            
            size_recurrent_weights = 1
            for index in layer_info['r_w'][0].shape:
                size_recurrent_weights *= index
            f.write(struct.pack('i', size_recurrent_weights))
            for recurrent_weight in layer_info['r_w'][0].flatten():
                f.write(struct.pack('f', recurrent_weight))

print("File created with parameters of", filenameIn, "in", filenameOutCfg)

## HDF5 to JSON

import h5py
import numpy as np
import json
import pandas as pd

# Data to be written
dictionary ={}

def isGroup(obj):
    if isinstance(obj,h5py.Group):
        return True
    return False

def getDatasetFromGroup(datasets_labels,datasets,obj,key_p):
    if isGroup(obj):
        for key in obj:
            x = obj[key]
            getDatasetFromGroup(datasets_labels,datasets,x,key)
    else:
        datasets_labels.append(key_p)
        datasets.append(obj)

def getModelJson(filename):
    with h5py.File(filename, 'r') as f:
        for key in f:
            obj = f[key]
            datasets = []
            datasets_labels = []
            dictionary[key] = {}
            getDatasetFromGroup(datasets_labels,datasets,obj,key)
            for dataset,dataset_label in zip(datasets,datasets_labels):
                w = np.array(dataset)
                if len(w.shape) == 4:
                   w = np.transpose(w, [3, 2, 0, 1])
                elif len(w.shape) == 2: # dense
                    w = np.transpose(w, [1, 0])
                dictionary[key][dataset_label] = {}
                dictionary[key][dataset_label]["dtype"] = str(w.dtype)
                dictionary[key][dataset_label]["shape"] = w.shape
                dictionary[key][dataset_label]["data"] = w.tolist()
    json_object = json.dumps(dictionary, indent = 4)

    with open(filenameOutJson, "w") as outfile:
        outfile.write(json_object)
        print("File created with parameters of", filenameIn, "in", filenameOutJson)

getModelJson(filenameIn)